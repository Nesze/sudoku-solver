package hu.boltz.nesze;

import hu.boltz.nesze.model.Sudoku;

public class Main {

    public static void main(String[] args) {
        Sudoku sudoku = new Sudoku("input4.txt");
        
        sudoku.solve();
        
        sudoku.printResults();
    }
}
