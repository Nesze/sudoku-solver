package hu.boltz.nesze.model;

import hu.boltz.nesze.exception.BoardNotFilledException;
import hu.boltz.nesze.exception.InvalidInputException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Board {
	
	private int boardWidth;
	private int boardHeight;
	private int regionWidth;
	private int regionHeight;
	
	private int[][] cells;
	private int cellsFilled;
	
	public Board(int boardWidth, int boardHeight, int regionWidth, int regionHeight) {
		this.cells = new int[boardWidth][boardHeight];
		cellsFilled = 0;
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
	}
	
	public void InitFromFile(String path) throws IOException, InvalidInputException {
		List<List<String>> input = Arrays
				.stream(new String(Files.readAllBytes(Paths.get(path)))
						.split("\n"))
				.map(line -> Arrays
						.stream(line.split(" "))
						.map(String::trim)
						.collect(Collectors.toList()))
				.collect(Collectors.toList());
		if (input.size() != boardHeight) {
			throw new InvalidInputException();
		}
		for (int x = 0; x < boardWidth; ++x) {
			if (input.get(x).size() != boardHeight) {
				throw new InvalidInputException();
			}
			for (int y = 0; y < boardHeight; ++y) {
				try {
					setCell(new Coords(x, y), Integer.parseInt(input.get(x).get(y)));
				} catch (NumberFormatException e) {
					throw new InvalidInputException();
				}
			}
		}
	}
	
	public Integer getCell(Coords c) {
		return getCell(c.x, c.y);
	}
	
	public Integer getCell(int x, int y) {
		return cells[x][y];
	}
	
	public void setCell(Coords c, int value) {
		if (getCell(c) == 0 && value != 0) {
			cellsFilled++;
		} else if (getCell(c) != 0 && value == 0) {
			cellsFilled--;
		}
		cells[c.x][c.y] = value;
	}
	
	public boolean existsInColumn(int x, int value) {
		for (int cY = 0; cY < boardHeight; ++cY) {
			if (getCell(x, cY) == value) {
				return true;
			}
		}
		return false;
	}
	
	public boolean existsInRow(int y, int value) {
		for (int cX = 0; cX < boardWidth; ++cX) {
			if (getCell(cX, y) == value) {
				return true;
			}
		}
		return false;
	}
	
	public boolean existsInRegion(Coords rC, int value) {
		for (int cX = rC.x * regionWidth; cX < (rC.x + 1) * regionWidth; ++cX) {
			for (int cY = rC.y * regionHeight; cY < (rC.y + 1) * regionHeight; ++cY) {
				if (cells[cX][cY] == value) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isFilled() {
		return cellsFilled == boardWidth * boardHeight;
	}
	
	private boolean areColumnsValid() {
		for (int x = 0; x < boardWidth; ++x) {
			List<Integer> column = new ArrayList<>();
			for (int y = 0; y < boardHeight; ++y) {
				if (column.contains(cells[x][y])) {
					return false;
				}
				column.add(cells[x][y]);
			}
		}
		return true;
	}
	
	private boolean areRowsValid() {
		for (int y = 0; y < boardHeight; ++y) {
			List<Integer> row = new ArrayList<>();
			for (int x = 0; x < boardWidth; ++x) {
				if (row.contains(cells[x][y])) {
					return false;
				}
				row.add(cells[x][y]);
			}
		}
		return true;
	}
	
	private boolean areRegionsValid() {
		for (int rX = 0; rX < boardWidth / regionWidth; ++rX) {
			for (int rY = 0; rY < boardHeight / regionHeight; ++rY) {
				List<Integer> region = new ArrayList<>();
				for (int x = rX * regionWidth; x < (rX + 1) * regionWidth; ++x) {
					for (int y = rY * regionHeight; y < (rY + 1) * regionHeight; ++y) {
						if (region.contains(cells[x][y])) {
							return false;
						}
						region.add(cells[x][y]);
					}
				}
			}
		}
		return true;
	}
	
	public boolean isFinished() throws BoardNotFilledException {
		// Checking if board is filled entirely
		if (!isFilled()) {
			throw new BoardNotFilledException();
		}
		// Checking if columns, rows and regions are filled correctly
		return areColumnsValid() && areRowsValid() && areRegionsValid();
	}
	
	@Override
	public String toString() {
		StringBuilder board = new StringBuilder();
		for (int x = 0; x < boardWidth; ++x) {
			if (x % regionWidth == 0) {
				board.append("\n");
			}
			for (int y = 0; y < boardHeight; ++y) {
				if (y % regionHeight == 0) {
					board.append("  ");
				}
				board.append(getCell(x, y) != 0 ? getCell(x, y) : " ").append(" ");
			}
			board.append("\n");
		}
		return board.toString();
	}

}
