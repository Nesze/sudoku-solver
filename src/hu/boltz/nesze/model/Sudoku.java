package hu.boltz.nesze.model;

import hu.boltz.nesze.exception.BoardNotFilledException;
import hu.boltz.nesze.exception.InvalidInputException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Sudoku {
	
	private static final int BOARD_SIZE = 9;
	private static final int REGION_SIZE = 3;
	
	private Board board;
	
	public Sudoku(String path) {
		try {
			board = new Board(BOARD_SIZE, BOARD_SIZE, REGION_SIZE, REGION_SIZE);
			board.InitFromFile(path);
		} catch (InvalidInputException e) {
			System.out.println("Invalid input format");
		} catch (IOException e) {
			System.out.println("File not found");
		}
	}
	
	
	public void solve() {
		while (!board.isFilled()) {
			boolean cycleSuccessful = performMultiRegionCycle();
			if (!cycleSuccessful) {
				cycleSuccessful = performColumnCycle();
			}
			if (!cycleSuccessful) {
				cycleSuccessful = performRowCycle();
			}
			if (!cycleSuccessful) {
				break;
			}
		}
	}
	
	public void printResults() {
		try {
			if (board.isFinished()) {
				System.out.println("Board solved");
			} else {
				System.out.println("Board solved incorrectly");
			}
		} catch (BoardNotFilledException e) {
			System.out.println("Board not solved entirely");
		}
		System.out.println(board.toString());
	}
	
	private boolean performMultiRegionCycle() {
		boolean boardChanged = false;
		Coords rC = new Coords();
		for (int currentNumber = 1; currentNumber <= BOARD_SIZE; ++currentNumber) {
			for (rC.x = 0; rC.x < BOARD_SIZE / REGION_SIZE; ++(rC.x)) {
				for (rC.y = 0; rC.y < BOARD_SIZE / REGION_SIZE; ++(rC.y)) {
					boardChanged |= multiRegionAttempt(rC, currentNumber);
				}
			}
		}
		return boardChanged;
	}
	
	private boolean multiRegionAttempt(Coords rC, int value) {
		if (board.existsInRegion(rC, value)) {
			return false;
		}
		List<Coords> availableCellsInRegion = findEmptyCellsInRegion(rC);
		
		List<Integer> occupiedColumns = new ArrayList<>();
		for (int x = rC.x * REGION_SIZE; x < (rC.x + 1) * REGION_SIZE; ++x) {
			if (board.existsInColumn(x, value)) {
				occupiedColumns.add(x);
			}
		}
		availableCellsInRegion.removeIf(coords -> occupiedColumns.contains(coords.x));
		
		List<Integer> occupiedRows = new ArrayList<>();
		for (int y = rC.y * REGION_SIZE; y < (rC.y + 1) * REGION_SIZE; ++y) {
			if (board.existsInRow(y, value)) {
				occupiedRows.add(y);
			}
		}
		availableCellsInRegion.removeIf(coords -> occupiedRows.contains(coords.y));
		
		if (availableCellsInRegion.size() == 1) {
			board.setCell(availableCellsInRegion.get(0), value);
			return true;
		}
		return false;
	}
	
	private boolean performColumnCycle() {
		boolean boardChanged = false;
		for (int x = 0; x < BOARD_SIZE; ++x) {
			List<Coords> availableCoordsInColumn = new ArrayList<>();
			List<Integer> availableNumbersInColumn = new ArrayList<>();
			for (int i = 1; i <= BOARD_SIZE; ++i) {
				availableNumbersInColumn.add(i);
			}
			for (int y = 0; y < BOARD_SIZE; ++y) {
				if (board.getCell(x, y) == 0) {
					availableCoordsInColumn.add(new Coords(x, y));
				}
				availableNumbersInColumn.remove(board.getCell(x, y));
			}
			if (availableCoordsInColumn.size() == 1) {
				int missingNumber = availableNumbersInColumn.get(0);
				if (!board.existsInColumn(availableCoordsInColumn.get(0).x, missingNumber) && !board.existsInRow(availableCoordsInColumn.get(0).y, missingNumber)) {
					board.setCell(availableCoordsInColumn.get(0), missingNumber);
					boardChanged = true;
				}
			}
		}
		return boardChanged;
	}
	
	private boolean performRowCycle() {
		boolean boardChanged = false;
		for (int y = 0; y < BOARD_SIZE; ++y) {
			List<Coords> availableCoordsInRow = new ArrayList<>();
			List<Integer> availableNumbersInRow = new ArrayList<>();
			for (int i = 1; i <= BOARD_SIZE; ++i) {
				availableNumbersInRow.add(i);
			}
			for (int x = 0; x < BOARD_SIZE; ++x) {
				if (board.getCell(x, y) == 0) {
					availableCoordsInRow.add(new Coords(x, y));
				}
				availableNumbersInRow.remove(board.getCell(x, y));
			}
			if (availableCoordsInRow.size() == 1) {
				int missingNumber = availableNumbersInRow.get(0);
				if (!board.existsInColumn(availableCoordsInRow.get(0).x, missingNumber) && !board.existsInRow(availableCoordsInRow.get(0).y, missingNumber)) {
					board.setCell(availableCoordsInRow.get(0), missingNumber);
					boardChanged = true;
				}
			}
		}
		return boardChanged;
	}
	
	private List<Coords> findEmptyCellsInRegion(Coords rC) {
		List<Coords> emptyCellsInRegion = new ArrayList<>();
		for (int x = rC.x * REGION_SIZE; x < (rC.x + 1) * REGION_SIZE; ++x) {
			for (int y = rC.y * REGION_SIZE; y < (rC.y + 1) * REGION_SIZE; ++y) {
				if (board.getCell(x, y) == 0) {
					emptyCellsInRegion.add(new Coords(x, y));
				}
			}
		}
		return emptyCellsInRegion;
	}
	
}
