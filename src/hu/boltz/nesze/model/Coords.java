package hu.boltz.nesze.model;

public class Coords {

	public Coords() {
		this(null, null);
	}
	
	public Coords(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}
	
	public Integer x;
	
	public Integer y;
	
}
